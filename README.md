# Proposition d'une architecture réseau pour l'entreprise NIT

## Sommaire
- [Présentation](#présentation)  
- [Plan d'adressage](#plan-dadressage)  
    - [Site A B et C](#site-a-b-et-c)  
    - [Site Annexes](#site-annexes)  
    - [Site Datacenter](#site-datacenter)  
- [Architecture réseau](#architecture-réseau)  
- [Matériel necessaire par site](#matériel-necessaire-par-site)  
    - [Site A](#site-a)  
    - [Site B](#site-b)  
    - [Site C](#site-c)  
    - [Site Annexes](#site-annexes)  
    - [Site Datacenter](#site-datacenter)  
- [Configuration des équipements](#configuration-des-équipements)  
    - [Site A](#site-a-1)  
    - [Site B](#site-b-1)  
    - [Site C](#site-c-1)  
    - [Site Annexes](#site-annexes-1)  
    - [Site Datacenter](#site-datacenter-1)  
    - [Protocols utilisés](#protocols-utilisés)  
        - [STP](#stp)  
        - [VRRP](#vrrp)  
        - [Stack et Ring](#stack-et-ring)  
        - [VPN](#vpn)  
- [Proposition d'amélioration](#proposition-damélioration)  
    - [Amélioration de la sécurité et de la disponibilité](#amélioration-de-la-sécurité-et-de-la-disponibilité)
- [Cout total de l'architecture](#cout-total-de-larchitecture)

## Avant propos
Cette documentation a été réalisé dans le cadre d'une demande d'architecture réseau pour l'entreprise fictif NIT. Le shéma suivant nous a été fourni pour nous aider dans notre démarche: ![Architecture réseau](./img/schema_consigne.png)
La réponse à la demande de l'entreprise NIT proposé dans ce document est réalisé par Luc GARRABOS et Maxence GILLES.
## Présentation 

Dans ce document vous retrouverez la proposition d'une architecture réseau pour l'entreprise NIT, ainsi que la configuration des différents éléments du réseau.  
Les demandes du client sont les suivantes :
- 5 sites distants (A, B, C, Annexes, Datacenter)
    - A , B et C reliés entre eux par des liens privés (12 brins)
    - Accès internet pour les sites A, Annexes et Datacenter
- Site A :
    - 350 utilisateurs
    - 350 téléphones IP
    - 35 imprimantes
- Site B :
    - 200 utilisateurs
    - 200 téléphones IP
    - 20 imprimantes
- Site C :
    - 100 utilisateurs
    - 100 téléphones IP
    - 10 imprimantes
- Site Annexes :
    - 25 utilisateurs
    - 25 téléphones IP
    - 2 imprimantes
- Site Datacenter :
    - 30 serveurs (DMZ + Intranet)

- Création de Vlan
    - Vlan Poste de travail (client Bat A, B et C) (Vlan 10)
    - Vlan Poste de travail (client Bat Annexe) (Vlan 110)
    - Vlan Poste de travail (commerciaux Bat A, B et C) (Vlan 20)
    - Vlan Poste de travail (commerciaux Bat Annexe) (Vlan 120)
    - Vlan Poste de travail (direction Bat A, B et C) (Vlan 30)
    - VLAN Poste de travail (direction Annexe) (Vlan 130)
    - Vlan Telephone (Vlan 40 Bat A, B et C)
    - Vlan Telephone (Vlan 140 Bat Annexe)
    - Vlan Imprimante (Vlan 50 Bat A, B et C)
    - Vlan Imprimante (Vlan 150 Bat Annexe)
    - Vlan Poste de travail/Management (Admin + switch) (Vlan 100 Bat A, B et C)
    - VLAN Poste de travail/Management (Admin + switch) (Vlan 101 Bat Annexe)
    - Vlan serveur Web (Vlan 200)
    - Vlan serveur intra (Vlan 250)

## Plan d'adressage
Première adresse : 192.168.0.1  
Dernière adresse : 192.168.255.254  
Adresse de broadcast : 192.168.255.255  
Nombre d'adresses IP disponibles : 65534 

### Site A B et C :
Site A :
- Vlan 10 :
    - Masque de réseau : 255.255.254.0
    - Adresse de réseau : 192.168.10.0
    - Première adresse : 192.168.10.1
    - Dernière adresse : 192.168.11.253
    - Adresse de broadcast : 192.168.11.255
    - Nombre d'adresses IP disponibles : 509
    - Gateway : 192.168.11.254
- Vlan 20 :
    - Masque de réseau : 255.255.255.224 
    - Adresse de réseau : 192.168.20.0 /27
    - Première adresse : 192.168.20.1
    - Dernière adresse : 192.168.20.29
    - Adresse de broadcast : 192.168.20.31
    - Nombre d'adresses IP disponibles : 29
    - Gateway : 192.168.20.30
- Vlan 30 :
    - Masque de réseau : 255.255.255.240
    - Adresse de réseau : 192.168.30.0
    - Première adresse : 192.168.30.1
    - Dernière adresse : 192.168.30.13
    - Adresse de broadcast : 192.168.30.15
    - Nombre d'adresses IP disponibles : 13
    - Gateway : 192.168.30.14
- Vlan 40 :
    - Adresse de réseau : 192.168.40.0
    - Première adresse : 192.168.40.1
    - Dernière adresse : 192.168.43.253
    - Adresse de broadcast : 192.168.43.255
    - Nombre d'adresses IP disponibles : 1021
    - Gateway : 192.168.43.254
- Vlan 50 :
    - Masque de réseau : 255.255.255.192
    - Adresse de réseau : 192.168.50.0
    - Première adresse : 192.168.50.1
    - Dernière adresse : 192.168.50.65
    - Adresse de broadcast : 192.168.50.67
    - Nombre d'adresses IP disponibles : 65
    - Gateway : 192.168.50.66
- Vlan 100 :
    - Masque de réseau : 255.255.255.248
    - Adresse de réseau : 192.168.100.0
    - Première adresse : 192.168.100.1
    - Dernière adresse : 192.168.100.5
    - Adresse de broadcast : 192.168.100.7
    - Nombre d'adresses IP disponibles : 5
    - Gateway : 192.168.100.6

### Site Annexes :
- Vlan 110 :
    - Masque de réseau : 255.255.255.224
    - Adresse de réseau : 192.168.110.0
    - Première adresse : 192.168.110.1
    - Dernière adresse : 192.168.110.29
    - Adresse de broadcast : 192.168.110.31
    - Nombre d'adresses IP disponibles : 29
    - Gateway : 192.168.110.30
- Vlan 120 :
    - Masque de réseau : 255.255.255.240
    - Adresse de réseau : 192.168.120.0
    - Première adresse : 192.168.120.1
    - Dernière adresse : 192.168.120.13
    - Adresse de broadcast : 192.168.120.15
    - Nombre d'adresses IP disponibles : 13
    - Gateway : 192.168.120.14
- Vlan 130 :
    - Masque de réseau : 255.255.255.240
    - Adresse de réseau : 192.168.130.0
    - Première adresse : 192.168.130.1
    - Dernière adresse : 192.168.130.13
    - Adresse de broadcast : 192.168.130.15
    - Nombre d'adresses IP disponibles : 13
    - Gateway : 192.168.130.14
- Vlan 140 :
    - Masque de réseau : 255.255.255.224
    - Adresse de réseau : 192.168.140.0
    - Première adresse : 192.168.140.1
    - Dernière adresse : 192.168.140.29
    - Adresse de broadcast : 192.168.140.31
    - Nombre d'adresses IP disponibles : 29
    - Gateway : 192.168.140.30
- Vlan 150 :
    - Masque de réseau : 255.255.255.248
    - Adresse de réseau : 192.168.150.0
    - Première adresse : 192.168.150.1
    - Dernière adresse : 192.168.150.5
    - Adresse de broadcast : 192.168.150.7
    - Nombre d'adresses IP disponibles : 5
    - Gateway : 192.168.150.6
- Vlan 101 :
    - Masque de réseau : 255.255.255.248
    - Adresse de réseau : 192.168.101.0
    - Première adresse : 192.168.101.1
    - Dernière adresse : 192.168.101.5
    - Adresse de broadcast : 192.168.101.7
    - Nombre d'adresses IP disponibles : 5
    - Gateway : 192.168.101.6

### Site Datacenter :
- Vlan 200 :
    - Masque de réseau : 255.255.255.224
    - Adresse de réseau : 192.168.200.0
    - Première adresse : 192.168.200.1
    - Dernière adresse : 192.168.200.30
    - Adresse de broadcast : 192.168.200.31
    - Nombre d'adresses IP disponibles : 30
- Vlan 250 :
    - Masque de réseau : 255.255.255.224
    - Adresse de réseau : 192.168.250.0
    - Première adresse : 192.168.250.1
    - Dernière adresse : 192.168.250.30
    - Adresse de broadcast : 192.168.250.31
    - Nombre d'adresses IP disponibles : 30

## Architecture réseau

![Architecture réseau](./img/image.png)

On y voit en Jaune Le batiment A  
En Bleu les batiments B et C  
En Vert Le Datacenter  
Et en marron l'annexe  

 Nous avons pensé à rajouter 2 lien fibres entre les batiments B et C afin de garantir un connexion au équipe de travail du batiment C en cas de panne du lien avec le batiment A.   
 De plus, si les batiments B et C sont relié, et que le batiment B avec une fibre opérateur, cela permettrait de garantir une connexion au batiment C en cas de panne du lien avec le batiment A. 

## Matériel necessaire par site

Dans le matériel que nous avons choisi pour le réseau de l'entreprise NIT, nous avons choisi d'utiliser des switchs de la marque Aruba, et les firewalls de la marque Fortinet.

#### Site A
2 Fortinet FortiGate 600E   
8 Aruba 8360-48XT4C 4p 100G QSFP+  
2 Aruba 8360-32Y4C  
1 HPE FlexNetwork 5140 48G POE+ 
2 bornes wifi Aruba AP-515

#### Site B  
5 Aruba 8360-48XT4C 4p 100G QSFP+  
2 Aruba 8360-32Y4C 

#### Site C
3 Aruba 8360-48XT4C 4p 100G QSFP+  
2 Aruba 8360-32Y4C

#### Site Annexes
1 HPE FlexNetwork 5140 48G POE+  
1 Fortinet FortiGate 600E
2 bornes wifi Aruba AP-515

#### Site Datacenter
4 Fortinet FortiGate 600E  
4 Aruba 8360-48XT4C 4p 100G QSFP+

## Configuration des équipements

#### Site A

Les Firewall  FortiGate 600E sont configurés pour etre utiliser en tant que firewall. Ils sont donc mis en Cluster. Ceci permet d'avoir une redondance en cas de panne d'un des firewall.

Les switch Aruba 8360-48XT4C 4p 100G QSFP+ sont configurés pour etre utiliser en tant que switch d'acces. Ils sont donc mis en Stack et en Ring. Ceci permet d'avoir une redondance en cas de panne d'un des switchs.
Ils permettent de connecter les utilisateurs, les téléphones IP et les imprimantes. 

Les switch Aruba 8360-32Y4C sont configurés pour etre utiliser en tant que switch de distribution. Ils sont donc mis en Cluster et en Ring. Ceci permet d'avoir une redondance en cas de panne d'un des switchs.

Le switch HPE FlexNetwork 5140 48G POE+ est configuré pour etre utiliser en tant que switch de d'acces. Il permet de connecter les bornes wifi grace aux ports POE+.

Ajout de deux borne wifi Aruba AP-515 pour permettre aux collaborateurs et aux visiteurs de se connecter au réseau via le wifi.

Sur chacun des switch les protocols STP et VRRP sont activés. Ces derniers permettent d'avoir une redondance en cas de panne d'un des switchs pour garder une continuité de service.

#### Site B

Les switch Aruba 8360-48XT4C 4p 100G QSFP+ sont configurés pour etre utiliser en tant que switch d'acces. Ils sont donc mis en Stack et en Ring. Ceci permet d'avoir une redondance en cas de panne d'un des switchs.
Ils permettent de connecter les utilisateurs, les téléphones IP et les imprimantes.

Les switch Aruba 8360-32Y4C sont configurés pour etre utiliser en tant que switch de distribution. Ils sont donc mis en Cluster et en Ring. Ceci permet d'avoir une redondance en cas de panne d'un des switchs.

Sur chacun des switch les protocols STP et VRRP sont activés. Ces derniers permettent d'avoir une redondance en cas de panne d'un des switchs pour garder une continuité de service.

#### Site C

Les switch Aruba 8360-48XT4C 4p 100G QSFP+ sont configurés pour etre utiliser en tant que switch d'acces. Ils sont donc mis en Stack et en Ring. Ceci permet d'avoir une redondance en cas de panne d'un des switchs.

Les switch Aruba 8360-32Y4C sont configurés pour etre utiliser en tant que switch de distribution. Ils sont donc mis en Cluster et en Ring. Ceci permet d'avoir une redondance en cas de panne d'un des switchs.

Sur chacun des switch les protocols STP et VRRP sont activés. Ces derniers permettent d'avoir une redondance en cas de panne d'un des switchs pour garder une continuité de service.

#### Site Annexes

Le switch HPE FlexNetwork 5140 48G POE+ est configuré pour etre utiliser en tant que switch de d'acces. Il permet de connecter les bornes wifi grace aux ports POE+, et les utilisateurs, les téléphones IP et les imprimantes avec les port RJ45 classiques.

Ajout de deux borne wifi Aruba AP-515 pour permettre aux collaborateurs et aux visiteurs de se connecter au réseau via le wifi.

Le Firewall FortiGate 600E est configuré pour etre utiliser en tant que firewall. Il permet de sécuriser le réseau de l'annexe.

#### Site Datacenter

Les Firewall  FortiGate 600E sont configurés pour etre utiliser en tant que firewall. Ils sont donc mis en Cluster. Ceci permet d'avoir une redondance en cas de panne d'un des firewall.
Un des 2 cluster est pour la DMZ et l'autre pour l'infra interne de l'entreprise et un possible intranet.

Les switch Aruba 8360-48XT4C 4p 100G QSFP+ sont configurés pour etre utiliser en tant que switch d'acces. Ils sont donc mis en Stack et en Ring. Ceci permet d'avoir une redondance en cas de panne d'un des switchs.

### Protocols utilisés

##### STP
Le protocol STP (Spanning Tree Protocol) permet de créer un arbre recouvrant pour éviter les boucles dans un réseau. Il permet de créer le chemin le plus rapide pour arrivé a destination de la requete, il peut se faire automatiquement ou peux etre configuré manuellement.

##### VRRP
Le protocol VRRP (Virtual Router Redundancy Protocol) permet de créer un routeur virtuel. Il permet de créer un routeur virtuel qui permet de rediriger le traffic vers un routeur physique en cas de panne de ce dernier.

### Stack et Ring

Le stack permet de mettre en relation plusieurs switchs pour n'en faire qu'un seul. Cela permet de n'avoir qu'une seule configuration a faire pour tous les switchs reliés entre eux.

Le ring permet  d'avoir une redondance en cas de panne d'un des switchs et peut etre utilisé avec le stack.

### VPN

Le VPN (Virtual Private Network) permet de créer un réseau privé virtuel. Il permet de créer un tunnel sécurisé entre deux réseaux distants. Il permet de sécuriser les données qui transitent entre les deux réseaux.
Il est ici utiliser pour que les collaborateurs puissent accéder au réseau de l'entreprise depuis l'extérieur, ou que les administrateurs puissent accéder au réseau de management depuis chacun des sites et de l'exterieur, sans que n'importe qui puissent y accéder ou de laisser une route ouverte dans internet pour les controler.

## Proposition d'amélioration

### Amélioration de la sécurité et de la disponibilité

Nous avons eu comme idée pour améliorer la disponibilité de vous proposer de rajouter, dans un premier temps, de tirer une nouvelle fibre opérateur pour avoir une redondance sur le lien internet au batiment A qui est le coeur du réseau.
![Architecture réseau](./img/propos3.png)

Dans un second temps, nous avons pensé à rajouter un lien internet au batiment B comme c'est le 2 eme plus gros en personelle et en matériel. Cela permettrait d'avoir une redondance sur le lien internet au batiment B. Il faudrais donc rajouter un firewall au batiment B, voir un cluster de firewall pour plus de sécurité et de redondance.
![Architecture réseau](./img/propos2.png)

Pour améliorer la sécurité de l'annexe nous pensions a rajouter un firewall, afin d'avoir un cluster de firewall pour plus de sécurité et de redondance, si l'activité au niveau de l'annexe venait à augmenter.
![Architecture réseau](./img/propos1.png)

## Cout total de l'architecture

D'apres nos estimations, le cout total de l'architecture est de 1 452 000 €, pour les switch et les firewall avec licenses.  
Il manque donc le cout des fibres optiques et le cout de la main d'oeuvre pour l'installation et la configuration des équipements, que nous pourrons étudier sur place avec un approche plus précise et avec plus de détails sur le réseau actuel. Ainsi que la mise en place de rack pour les switchs et les firewalls.

Avec les conseils que nous vous avons donné pour améliorer la sécurité et la disponibilité du réseau, le cout total de l'architecture serait de 1 552 000 €.